## [1.0.0] – 2023-07-24

### Added
- Helper methods to get info about command-line arguments
- Docs

# Valhalla: Command Line Arguments
## About

Helper library, that handles command-line arguments parsing.

Works with
- Flags (e.g. `-f`, `--flag`)
- Key-Value pairs (e.g. `-k value`, `--key value`, ``-key=value``, ``--key=value``)

## How to use

Just use static methods from `Valhalla.CommandLineArguments`:
-`.IsFlagSet(key)`
-`.IsKeyValueSet(key)`
-`.GetKeyValue(key)` (can return `null`)

Also, you can use properties `DictionaryArgs` and `FlagArgs` to iterate over values yourself.

Keep in mind, that:
- `--key=value` is the same as `--key value`
- `-key` is the same as `--key`
- Use must use one or two dashes in command line arguments, but not in code (`key` is fine too)

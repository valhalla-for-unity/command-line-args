using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;


namespace Valhalla.CommandLineArguments
{
	public static class CommandLineArgs
	{
		private static Dictionary<string, string> _dictionaryArgs = new();
		private static HashSet<string> _flagArgs = new();
		
		private static bool _isSetup = false;


		public static IReadOnlyDictionary<string, string> DictionaryArgs
		{
			get
			{
				if (!_isSetup)
					Setup();

				return _dictionaryArgs;
			}
		}
		

		public static IReadOnlyCollection<string> FlagArgs
		{
			get
			{
				if (!_isSetup)
					Setup();

				return _flagArgs;
			}
		}


		private static void Setup()
		{
			_isSetup = true;
			
			var args = System.Environment.GetCommandLineArgs();

			for (int i = 0; i < args.Length; i++)
			{
				var arg = args[i];

				if (IsArgumentKey(arg))
				{
					arg = TrimArgumentKey(arg);

					if (!arg.Contains('='))
					{
						if (args.Length > i + 1)
						{
							var nextArg = args[i + 1];

							if (!IsArgumentKey(nextArg))
							{
								AddKeyArg(arg, nextArg);
								i++;
							}
							else
								_flagArgs.Add(arg);
						}
						else
							_flagArgs.Add(arg);
					}
					else
					{
						int index = arg.IndexOf('=');
						var key = arg.Substring(0, index);
						var value = arg.Substring(index + 1);

						AddKeyArg(key, value);
					}
				}
				else
					Debug.LogError($"CLI argument <{arg}> is not a key, nor a value after a key");
			}
		}


		public static bool IsFlagSet(string key)
		{
			key = TrimArgumentKey(key);
			
			return FlagArgs.Contains(key);
		}

		
		public static bool IsKeyValueSet(string key)
		{
			key = TrimArgumentKey(key);

			return DictionaryArgs.ContainsKey(key);
		}


		[CanBeNull]
		public static string GetKeyValue(string key)
		{
			key = TrimArgumentKey(key);

			if (IsKeyValueSet(key))
				return DictionaryArgs[key];

			return null;
		}
		

		private static void AddKeyArg(string key, string value)
		{
			if (_dictionaryArgs.ContainsKey(key))
				Debug.LogError($"CLI argument key already exists: $<{key}>=<{_dictionaryArgs[key]}>, new value <{value}> is skipped");
			else
				_dictionaryArgs.Add(key, value);
		}


		private static bool IsArgumentKey(string arg)
			=> arg.StartsWith('-');
		
		
		private static string TrimArgumentKey(string arg)
			=> arg.TrimStart('-');
	}
}
